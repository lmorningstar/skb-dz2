/* eslint no-console: ["error", { allow: ["log"] }] */
import express from 'express';

const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/canonize', (req, res) => {
  let { username } = req.query;
  console.log(username);
  const re = new RegExp('(\@)?(https?:)?(\/+)?(www.)?((telegram|vk|twitter|github|[a-z0-9-.])[^\/]*\/)?(\@?)?([a-z._]*)');
  const usernameRe = username.match(re);
  console.log(usernameRe);
  return res.send(`@${usernameRe.pop()}`);
});

app.get('/', (req, res) => {
  let { a, b } = req.query;
  if (!a) { a = 0; }
  if (!b) { b = 0; }
  a = parseInt(a, 10);
  b = parseInt(b, 10);
  const y = a + b;

  return res.json(y);
});

function checkString(str) {
  if (str.search(/[0-9_/]/) !== -1) {
    return true;
  }

  return false;
}

app.get('/fio', (req, res) => {
  let { fullname } = req.query;
  fullname = fullname.trim();
  const initials = fullname.split(/[\s,]+/);
  if (initials.length > 3 || !fullname || checkString(fullname)) {
    return res.send('Invalid fullname');
  }

  let answer = '';
  let f = '';

  switch (initials.length) {
    case 1:
      answer = initials[0].charAt(0).toUpperCase() + initials[0].substr(1).toLowerCase();
      break;
    case 2:
      f = initials[1].charAt(0).toUpperCase() + initials[1].substr(1).toLowerCase();
      answer = `${f} ${initials[0][0].toUpperCase()}.`;
      break;
    default:
      f = initials[2].charAt(0).toUpperCase() + initials[2].substr(1).toLowerCase();
      answer = `${f} ${initials[0][0].toUpperCase()}. ${initials[1][0].toUpperCase()}.`;
      break;
  }

  return res.send(answer);
});


app.listen(3000, () => {
  console.log('App listening on port 3000!');
});
